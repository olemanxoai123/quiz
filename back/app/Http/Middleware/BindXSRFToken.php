<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BindXSRFToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set('X-XSRF-TOKEN', $request->cookie('XSRF-TOKEN'));
        return $next($request);
    }
}
